FROM fedora

RUN dnf -y update
RUN dnf group install -y gnome-desktop base-x
RUN dnf install tigervnc-server -y
RUN dnf -y install sudo wget curl aria2 nano zip unzip p7zip p7zip-gui p7zip-plugins
RUN dnf -y install tilix terminator
RUN dnf -y install chrome-gnome-shell
RUN sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
RUN dnf -y update
RUN cd /usr/local/bin && wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip && unzip *.zip && rm -rf *.zip
RUN echo 'sudo ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
RUN echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
RUN echo '%sudo ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
RUN echo 'wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
RUN dnf -y clean all && rm -rf /tmp/*

#ENTRYPOINT ["/usr/lib/systemd/systemd"]